
public class Persona {

    private String name;
    private int edad;
    private String email;
    private int id;

    public String getName() {
        return this.name;
    }

    public Persona(String name, int edad, String email, int id) {
        this.name = name;
        this.edad = edad;
        this.email = email;
        this.id = id;
    }

    public Persona() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEdad() {
        return this.edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        Persona pers2 = (Persona) obj;
        Persona pers1 = this;

        if(pers1.getEdad() > pers2.getEdad()){
            System.out.println(pers1.getName()+
                " ("+pers1.getEdad()+
                ") es mayor que "+pers2.getName()+
                " ("+pers2.getEdad()+")");
                return false;
        }
        else if(pers1.getEdad() < pers2.getEdad()){
            System.out.println(pers1.getName()+
                " ("+pers1.getEdad()+
                ") es menor que "+pers2.getName()+
                " ("+pers2.getEdad()+")");
                return false;
        }
        else{
            System.out.println(pers1.getName()+
                " ("+pers1.getEdad()+
                ") y "+pers2.getName()+
                " ("+pers2.getEdad()+") tienen la misma edad.");
                return true;
        }

    }
    
}