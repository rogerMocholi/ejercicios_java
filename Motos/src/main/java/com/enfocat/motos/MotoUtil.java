package com.enfocat.motos;

public class MotoUtil {
    
public void informe(Moto[] motos){
    Moto masCara = masCara(motos);
    Moto masPot = masPotente(motos);
    Double precioTotal = sumaPrecios(motos);

    System.out.println("\nLa moto mas cara de todas es la "+masCara+"("+masCara.getPrecio()+" eur)");
    System.out.println("La moto mas potente de todas es la "+masPot+"("+masPot.getPotencia()+" cv)");
    System.out.println("El precio conjunto de las motos es de "+precioTotal);
}

public void compara(Moto m1, Moto m2){
    if (m1.getPotencia()>m2.getPotencia()) {
        System.out.println("La "+m1+"tiene "+(m1.getPotencia()-m2.getPotencia())+"cv mas que la "+m2);
    } else if(m2.getPotencia()>m1.getPotencia()){
        System.out.println("La "+m2+"tiene "+(m2.getPotencia()-m1.getPotencia())+"cv mas que la "+m1);

    }else{
        System.out.println("La "+m1+" y la "+m2+" tienen la misma potencia");
    }

    if (m1.getPrecio()>m2.getPrecio()) {
        System.out.println("La "+m1+"tiene "+(m1.getPrecio()-m2.getPrecio())+"eur mas que la "+m2);
    } else if(m2.getPrecio()>m1.getPrecio()){
        System.out.println("La "+m2+"tiene "+(m2.getPrecio()-m1.getPrecio())+"eur mas que la "+m1);

    }else{
        System.out.println("La "+m1+" y la "+m2+" tienen el mismo precio");
    }
}

public Moto masPotente(Moto[] motos){
    Moto maxP = null;
    for (Moto m : motos) {
        if(m.getPotencia()>maxP.getPotencia() || maxP.equals(null)){
            maxP = m;
        }
    }

    return maxP;

}

public Moto masCara(Moto[] motos){
    Moto maxP = null;
    for (Moto m : motos) {
        if(m.getPrecio()>maxP.getPrecio() || maxP.equals(null)){
            maxP = m;
        }
    }

    return maxP;
}

public Double sumaPrecios(Moto[] motos){
    Double precio = 0.0;
    for (Moto m : motos) {
        precio = precio+m.getPrecio();
    }

    return precio;
}

}