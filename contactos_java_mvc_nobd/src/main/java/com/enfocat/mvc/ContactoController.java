package com.enfocat.mvc;

import java.util.List;


public class ContactoController {


    // getAll devuelve la lista completa
    public static List<Contacto> getAll(){
        return Datos.getContactos();
    }

    //getId devuelve un registro
    public static Contacto getId(int id){
        return Datos.getContactoId(id);
    }
   
    //save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Contacto cn) {
        if (cn.getId()>0){
            Datos.updateContacto(cn);
        } else {
            Datos.newContacto(cn);
        }
        
    }

    // size devuelve numero de Contactos
    public static int size() {
        return Datos.getContactos().size();
    }


    // removeId elimina Contacto por id
    public static void removeId(int id){
       Datos.deleteContactoId(id);
    }

}