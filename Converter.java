import java.util.Scanner;

class Converter {

    public void aPorLaFaena(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Que tipo de unidad quieres convertir?");
        String input = sc.nextLine();
        System.out.println("A que tipo de unidad la quieres convertir?");
        String output = sc.nextLine();
        System.out.println("Que velocidad quieres convertir?");
        Double vel = sc.nextDouble();

        System.out.println("El resultado de la conversion es "+Convertidor(input, vel, output));


    }


    //CONVERSOR GENERAL
    public Double Convertidor(String in,Double doublein, String out){
        switch (in) {
            case "kmh":
                switch (out) {
                    case "ms":        
                        return kmH_2_mS(doublein);
                    
                    case "mih" :
                    return kmH_2_miH(doublein);

                    case "yds" :
                    return kmH_2_ydS(doublein);

                    default:
                        System.out.println("La unidad objetivo no es valida");
                        return null;
                }


                case "ms":
                    switch (out) {
                        case "kmh":        
                            return mS_2_kmH(doublein);
                        
                        case "mih" :
                            return mS_2_miH(doublein);

                        case "yds" :
                            return mS_2_ydS(doublein);

                        default:
                            System.out.println("La unidad objetivo no es valida");
                            return null;
                    }


                    case "mih":
                    switch (out) {
                        case "kmh":        
                            return miH_2_kmH(doublein);
                        
                        case "ms" :
                            return miH_2_mS(doublein);

                        case "yds" :
                            return miH_2_ydS(doublein);
                    
                        default:
                            System.out.println("La unidad objetivo no es valida");
                            return null;
                    }


                    case "yds":
                    switch (out) {
                        case "kmh":        
                            return ydS_2_kmH(doublein);

                        case "mih" :
                            return ydS_2_miH(doublein);

                        case "ms" :
                            return ydS_2_mS(doublein);
                    
                        default:
                            System.out.println("La unidad objetivo no es valida");
                            return null;
                    }
        
            default:
                System.out.println("La unidad a convertir no es valida.");
                return null;
        }

    }

    //CONVERSORES DE KM

    public Double kmH_2_mS(double km){
        double mS = (km*(1000/1)*(1.0/3600));
        return mS;

    }

    public Double kmH_2_miH(Double km){
        Double miH = (km*0.621371);
        return miH;

    }

    public Double kmH_2_ydS(Double km){
        Double ydS = ((km/1)*(1/1.61)*(1760/1.0)*(1.0/3600));
        return ydS;

    }

    //CONVERSORES DE M/S

    public Double mS_2_kmH(double mS){
        Double kmH = (mS/(3600/1000.0));
        return kmH;

    }

    public Double mS_2_miH(double mS){
        Double miH = (mS*2.23694);
        return miH;

    }

    public Double mS_2_ydS(double mS){
        Double ydS = (mS*1.09361);
        return ydS;

    }

    //CONVERSORES DE MI/H

    public Double miH_2_ydS(Double miH){
        Double ydS = (miH/2.04545);
        return ydS;

    }

    public Double miH_2_kmH(Double miH){
        Double kmH = (miH*1.60934);
        return kmH;

    }

    public Double miH_2_mS(Double miH){
        Double mS = (miH/2.237);
        return mS;

    }

    //CONVERSORES DE YD/S

    public Double ydS_2_miH(Double ydS){
        Double miH = (ydS*2.04545);
        return miH;

    }


    public Double ydS_2_kmH(Double ydS){
        Double kmH = (ydS*3.29184);
        return kmH;

    }

    public Double ydS_2_mS(Double ydS){
        Double mS = (ydS/1.09144);
        return mS;

    }


}