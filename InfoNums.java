

class InfoNums{


    public static int calculaMin(int[] numbs){
        int menor = numbs[0];
        for (int i : numbs) {
            if (i<menor) {
                menor = i;
            }
        }
        return menor;
    }

    public static int calculaMax(int[] numbs){
        int mayor = numbs[0];
        for (int i : numbs) {
            if (i>mayor) {
                mayor = i;
            }
        }
        return mayor;
    }

    public static int calculaMedia(int[] numbs){
        int total = 0;
        for (int i : numbs) {
            total = total + i;
        }
        int media = total/numbs.length;

        return media;
    }

    public static void main(String[] args) {
        int[] nums = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            nums[i] = Integer.parseInt(args[i]);
        }

        System.out.println(nums.length+" numeros introducidos");
        System.out.println("Minimo: "+calculaMin(nums));
        System.out.println("Maximo: "+calculaMax(nums));
        System.out.println("Media: "+calculaMedia(nums));

        
    }
}