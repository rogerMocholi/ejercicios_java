package com.enfocat.flota;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int opt = 0;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("\n\n\n\t\tBIENVENIDO AL GENERADOR DE FLOTAS");
            System.out.println("-----Opciones principales: ");
            System.out.println("\n1. Generar flota.");
            System.out.println("2. Mostrar resumen de la flota");
            System.out.println("-----Funciones de listado de coches de la flota: ");
            System.out.println("3. Filtrado por marca");
            System.out.println("4. Filtrado por color");
            System.out.println("5. Filtrado por Km");
            System.out.println("6. Filtro complejo");
            System.out.println("7. Salir");

            System.out.println("\nQue quieres hacer?(1/2/3/4/5/6/7)");
            opt = sc.nextInt();

            switch (opt) {
            case 1:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Cuantos coches quieres que tenga tu flota?");
                    int num = sc.nextInt();
                    Flota.generaCoches(num);
                    System.out.println("\nHecho");
                } else {
                    System.out.println("Ya has generado una flota, quieres hacer una nueva?(s/n)");
                    sc.nextLine();
                    if (sc.nextLine().equals("s")) {
                        System.out.println("Cuantos coches quieres que tenga tu flota?");
                        int num = sc.nextInt();
                        Flota.generaCoches(num);
                        System.out.println("\nHecho");
                    }

                }

                break;

            case 2:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Primero tienes que generar una flota.");
                } else {
                    Flota.resumen();
                }

                break;

            case 3:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Primero tienes que generar una flota.");
                } else {
                    System.out.println("Cual es la marca por la que quieres filtrar?(Renault,Dacia,Citroen,Seat)");
                    sc.nextLine();
                    String marc = sc.nextLine();
                    Flota.consultaMarca(marc);
                }

                break;

            case 4:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Primero tienes que generar una flota.");
                } else {
                    System.out.println("Cual es el color por el que quieres filtrar?(Rojo,Blanco,Negro,Amarillo)");
                    sc.nextLine();
                    String col = sc.nextLine();
                    Flota.consultaColor(col);
                }

                break;

            case 5:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Primero tienes que generar una flota.");
                } else {
                    System.out.println(
                            "Indica los Km y te enseñaremos una lista con los coches que tengan esos mismos Km o mas.");
                    int num = sc.nextInt();
                    Flota.consultaKm(num);
                }

                break;

            case 6:
                if (Flota.getCoches().isEmpty()) {
                    System.out.println("Primero tienes que generar una flota.");
                } else {
                    System.out.println(
                            "Este es el sistema de filtrado complejo, por favor, indica la marca que te interesa.");
                    sc.next();
                    String marc = sc.nextLine();
                    System.out.println("Introduce el color: ");
                    sc.next();
                    String col = sc.nextLine();
                    System.out.println("Introduce los km");
                    int km = sc.nextInt();
                    Flota.consultaMultiple(marc, col, km);
                }

                break;

            case 7:
                System.out.println("\n\n\nAgur!\n\n\n");

                break;

            default:
                System.out.println("Opcion no valida");
                break;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
       } while (opt != 7);
    }
}
