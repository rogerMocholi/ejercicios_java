package com.enfocat.flota;

public class Coche {
    private String marca;
    private String color;
    private int kms;
    private int anyo;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }


    @Override
    public String toString() {
        return this.marca+" "+this.color+" del año "+this.getAnyo()+" con "+this.kms+" de recorrido";
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public Coche(String marca, String color, int kms, int anyo) {
        this.marca = marca;
        this.color = color;
        this.kms = kms;
        this.anyo = anyo;
    }

    
}