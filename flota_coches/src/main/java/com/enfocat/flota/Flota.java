package com.enfocat.flota;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Flota {
    private static List<Coche> coches = new ArrayList<Coche>();
    private static String[] marcas = {"Renault","Dacia","Citroen","Seat"};
    private static String[] colores = {"Rojo","Blanco","Negro", "amarillo"};



    public static void generaCoches(int num){
        Random rand = new Random();
        coches.clear();
        for (int i = 0; i < num; i++) {
            Coche c = new Coche(marcas[rand.nextInt(4)], colores[rand.nextInt(4)], (rand.nextInt(150000)+1),(rand.nextInt(20)+2000));
            coches.add(c);
        }
    }

    public static void resumen(){
        MaxFrec mm = new MaxFrec();
        mm = marcaComun();
        System.out.println("------------RESUMEN-----------");
        System.out.println(coches.size()+" coches en la lista");
        System.out.println("La marca mas comun es "+mm.getValor()+" y tiene "+mm.getNum()+" coches.");
        mm = colorComun();
        System.out.println("El color mas frecuente es el "+mm.getValor()+" con "+mm.getNum()+" coches.");
        System.out.println(cuentaKm()+" coches tienen mas de 100.000 km");




    }

    private static MaxFrec marcaComun(){
        int[] numco = {0,0,0,0};
        int pos = 0;
        

        for (Coche coche : coches){
            // System.out.println(coche);
            
            if (coche.getMarca().equals("Renault")){
                numco[0]++;
            } else if (coche.getMarca().equals("Dacia")){
                numco[1]++;
            } else if (coche.getMarca().equals("Citroen")){
                numco[2]++;
            } else {
                //Seat
                numco[3]++;
            }           
        }
        int i = 0;
        for (int n : numco) {
            if(n>numco[pos]){
                pos=i;
            }
            i++;
        }
        MaxFrec mm = new MaxFrec(marcas[pos], numco[pos]);
        return mm;
    }

    private static MaxFrec colorComun(){
        int[] numco = {0,0,0,0};
        int pos = 0;
        

        for (Coche coche : coches){
            
            if (coche.getColor().equals("Rojo")){
                numco[0]++;
            } else if (coche.getColor().equals("Blanco")){
                numco[1]++;
            } else if (coche.getColor().equals("Negro")){
                numco[2]++;
            } else {
                //Amarillo
                numco[3]++;
            }           
        }
        int i = 0;
        for (int n : numco) {
            if(n>numco[pos]){
                pos=i;
            }
            i++;
        }
        MaxFrec mm = new MaxFrec(colores[pos], numco[pos]);
        return mm;
    }
    private static int cuentaKm(){
        int num=0;
        for (Coche c : coches) {
            if(c.getKms() >= 100000){
                num++;
            }
        }
        return num;
    }
    public static void consultaMarca(String marca){
        Boolean bol = false;
        for (Coche c : coches) {
            if(c.getMarca().equals(marca)){
                bol = true;
                System.out.println(c);
            }
        }
        if (!bol) System.out.println("No hay ningun coche de esta marca");
    }

    public static void consultaKm(int minKm){
        Boolean bol = false;
        for (Coche c : coches) {
            if(c.getKms()>=minKm){
                bol=true;
                System.out.println(c);
            }
        }
        if (!bol) System.out.println("No hay ningun coche con este kilometraje ni superior");
    }

    public static void consultaColor(String color){
        Boolean bol = false;
        for (Coche c : coches) {
            if(c.getColor().equals(color)){
                bol = true;
                System.out.println(c);
            }
        }
        if (!bol) System.out.println("No hay ningun coche de este color");
    }

    public static void consultaMultiple(String marca, String color, int minKm){
        for (Coche c : coches) {
            if(c.getMarca().equals(marca) && c.getColor().equals(color) && c.getKms()>=minKm){
                System.out.println(c);
            }
        }
    }

    public static List<Coche> getCoches() {
        return coches;
    }

    public static void setCoches(List<Coche> coches) {
        Flota.coches = coches;
    }
}