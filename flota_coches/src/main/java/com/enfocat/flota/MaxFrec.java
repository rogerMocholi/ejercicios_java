package com.enfocat.flota;
public class MaxFrec{

    private String valor;
    private int num;


    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }



    public MaxFrec() {
    }

    public MaxFrec(String valor, int num) {
        this.valor = valor;
        this.num = num;
    }

    



}