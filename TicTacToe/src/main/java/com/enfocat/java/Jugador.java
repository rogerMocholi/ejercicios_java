package com.enfocat.java;

public class Jugador {
    String nombre;
    private int empates;
    private int ganadas;
    private int partidas;

    public Jugador(String nombre) {
        this.nombre = nombre;
        this.empates = 0;
        this.ganadas = 0;
        this.partidas = 0;
    }
}
