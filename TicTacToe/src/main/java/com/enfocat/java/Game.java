package com.enfocat.java;

import java.util.Random;
import java.util.Scanner;

public class Game{
    private int[] map = {0,0,0,0,0,0,0,0,0};
    private static final int[][] winners = {
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {0,3,6},
        {1,4,7},
        {2,5,8},
        {0,4,8},
        {2,4,6}
    };

    public void setMap(int pos, int player){
        map[pos]=player;
    }

    public int getMap(int pos){

        return map[pos];
    }


    public String setSel(int pos){
        switch (map[pos]) {
            case 0:
                return "-";

            case 1: 
                return "X";

            case 2:
                return "O";

            default:
                return null;
        }

    }

    public void draw(int player){
        System.out.println("Turno del jugador "+player);

        System.out.println("\n\t"+setSel(0)+"\t|\t"+setSel(1)+"\t|\t"+setSel(2));
        System.out.println("--------------------------------------------------");
        System.out.println("\n\t"+setSel(3)+"\t|\t"+setSel(4)+"\t|\t"+setSel(5));
        System.out.println("--------------------------------------------------");
        System.out.println("\n\t"+setSel(6)+"\t|\t"+setSel(7)+"\t|\t"+setSel(8));



    }

    public int numZeros(){
        int n = 0;
        for (int p : map) {
            if (p == 0) n++;
        }
        return n;
    }

    public boolean winner(){
        for (int p = 1; p <= 2; p++) {
            for (int[] w : winners) {
                int coincidencias = 0;
                for (int winpos : w) {
                    if(getMap(winpos)==p){
                        coincidencias++;
                    }
                }
                if(coincidencias==3){
                    draw(p);
                    System.out.println("\nHA GANADO EL JUGADOR "+p);
                    return true;

                }
                
            }
        }
        return false;

    }

    public int IAplaysRand(){
        Random r = new Random();
        int opt = r.nextInt(9);
        do {
            if(map[opt] != 0){
                opt = r.nextInt(9);
            }
        } while (map[opt] != 0);

        return opt;
    }

    public void play(){
        boolean win=false;
        int player = 1;
        Scanner sc = new Scanner(System.in);
        int opt;
        do {
            draw(player);
            if(player==1){
                System.out.println("\nQue posicion quieres marcar?(1-9)");
 // //              opt = sc.nextInt()-1;
                do {
                    opt = sc.nextInt()-1;
                    if(map[opt] != 0){
                        System.out.println("Esta casilla ya esta marcada, elige otra");
                    }
                } while (map[opt] != 0);
            
            }else{
                System.out.println("\nVale, dejame que piense...");
                opt = IAplaysRand();
                System.out.println("\nEscojo la casilla numero "+opt+"\n");
            }
            map[opt]=player;
            win=winner();
            if(player==1) player=2;
            else player=1;
            
        } while (!win);
    }


}