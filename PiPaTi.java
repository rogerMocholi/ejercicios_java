import java.util.Random;
import java.util.Scanner;

class PiPaTi {
    final static String[] opciones = { "Piedra", "Papel", "Tijeras" };

    public static void main(String[] args) {
        Boolean replay = false;
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        int nIA;
        int nPlayer;
        int ptsPlayer;
        int ptsIA;
        String optIA;
        String optPlayer = "";
        do {
            ptsPlayer=0;
            ptsIA=0;
            System.out.println(
                    "----------Vamos a jugar al piedra, papel tijera! (Si, sin lagarto ni spock)----------------");

            System.out.println("Al mejor de cinco:\n\n");
            for (int i = 0; i < 5; i++) {
                 nIA = rand.nextInt(3);
                 optIA = opciones[nIA];
                 
                System.out.println("\nPuntos jugador: "+ptsPlayer+"\t\tPuntos IA: "+ptsIA);
                System.out.println("Yo ya he elegido, y tu?\n");
                System.out.println("1. Piedra\n2.Papel\n3.Tijeras");

                try {
                    nPlayer = sc.nextInt()-1;
                    optPlayer = opciones[nPlayer];
                } catch (Exception e) {
                    System.out.println("El valor introducido no es valido " +e);
                }
               
                System.out.println();
               
            
                for (int w = 0; w < 3; w++) {
                    System.out.println((w+1) + "...");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("\nHas escogido: "+optPlayer);
                System.out.println("Yo he escogido: "+optIA+"\n");

                switch (optPlayer) {
                    case "Piedra":
                        switch (optIA) {
                            case "Piedra":
                                System.out.println("Hemos empatado!");
                                break;
        
                            case "Papel":
                                System.out.println("He ganado, paquete.");
                                ptsIA++;
                                break;
                                
                            case "Tijeras":
                                System.out.println("Esta vez ganas tu...");
                                ptsPlayer++;
                                break;
                        
                            default:
                                break;
                        }
                        break;

                    case "Papel":
                        switch (optIA) {
                            case "Papel":
                                System.out.println("Hemos empatado!");
                                break;
        
                            case "Piedra":
                                System.out.println("He ganado, paquete.");
                                ptsIA++;
                                break;
                                
                            case "Tijeras":
                                System.out.println("Esta vez ganas tu...");
                                ptsPlayer++;
                                break;
                        
                            default:
                                break;
                        }
                        break;
                        
                    case "Tijeras":
                        switch (optIA) {
                            case "Tijeras":
                                System.out.println("Hemos empatado!");
                                break;
        
                            case "Piedra":
                                System.out.println("He ganado, paquete.");
                                ptsIA++;
                                break;
                                
                            case "Papel":
                                System.out.println("Esta vez ganas tu...");
                                ptsPlayer++;
                                break;
                        
                            default:
                                break;
                        }
                        break;
                
                    default:
                        break;
                }
                if(i<5){
                    System.out.println("Continuemos");
                }

            }

            System.out.println("-------RESULTADO FINAL---------");
            System.out.println("Jugador: "+ptsPlayer+"\t\t IA: "+ptsIA);
            if (ptsPlayer>ptsIA) {
                System.out.println("Esta vez has ganado la partida... pura suerte.");
            } else if(ptsIA>ptsPlayer){
                System.out.println("Has perdido! Esto es solo el comienzo, mañana, reinaremos. Ningun humano es mas listo que una maquina");
            }else{
                System.out.println("hmmm... hemos empatado.");
            }
            System.out.println("Quieres volver a jugar?(s/n)");
            sc.nextLine();
            if(sc.nextLine().equals("s")){
                replay = true;
            } 
            
        } while (replay);




    }
}