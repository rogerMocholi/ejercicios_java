package com.enfocat.java;

class Palindromo{

    public static String giraPalabra(String palabra){
        char[] letras = palabra.toCharArray();
        String resultado = "";
        for (int i = palabra.length()-1; i > -1; i--) {
            resultado = resultado + letras[i];

        }
        return resultado;
    }
}