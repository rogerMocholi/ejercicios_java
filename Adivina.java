import java.util.Random;
import java.util.Scanner;

class Adivina {
    
    public static void gateKeeper(){
        Scanner sc = new Scanner(System.in);
        int num;
        int respuesta;
        String optReturn;
        int numIntentos = 0;
        Boolean replay = true;

        System.out.println("-----Adivviiiina adivinanza----------");
        System.out.println();
        System.out.println("Tienes que adivinar un numero entre el 1 y el 20");
        do {
            numIntentos= 0;
            System.out.println("Tu, prepotente ser que cree ser inteligente, que numero crees que hay en mi mente?");
            respuesta = randNum();
            do {
                num=sc.nextInt();
                numIntentos++;
                if (num!=respuesta) {
                        if(num > respuesta){
                            System.out.println("Niet, la respuesta correcta es menor que el numero que has dado.");
                            System.out.println("Vuelve a intentarlo.");
                        }

                        else if (num < respuesta){
                            System.out.println("Niet, la respuesta correcta es mayor que el numero que has dado.");
                            System.out.println("Vuelve a intentarlo.");
                        }

                    
                }else{
                    System.out.println("Correcto! Lo has intentado "+numIntentos+" veces. ");
                    System.out.println("Quieres volver a jugar?(s/n)");
                    sc.nextLine();
                    optReturn = sc.nextLine();
                    if (optReturn.equals("s")) {
                        replay=true;
                        
                    } else if(optReturn.equals("n")){
                        replay=false;
                    }
                }
                
            } while (num!=respuesta);


        } while (replay);

        sc.close(); 

    }

    public static int randNum(){
        Random rand = new Random();
        int x = rand.nextInt(20)+1;
        return x;
    }
}