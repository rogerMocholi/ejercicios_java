
class TestPerson {
    public static void main(String[] args) {
        Persona per1 = new Persona("Roger", 22, "roger.mocholi@gmail.com", 1);
        Persona per2 = new Persona("Gina", 22, "nomesesucorreo@gmail.com", 2);
        Persona per3 = new Persona("Pau", 18, "elsuyotampoco@gmail.com", 3);
        Persona per4 = new Persona("Cynthia", 25, "nielsuyo@gmail.com", 4);

        PersonaController.MuestraPersona(per2);

        PersonaController.ComparaPersonas(per1, per2);
        PersonaController.ComparaPersonas(per1, per3);
        PersonaController.ComparaPersonas(per2, per4);

    }
}